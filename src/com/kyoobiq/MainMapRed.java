package com.kyoobiq;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import com.kyoobiq.mappers.FilterMapper;
import com.kyoobiq.mappers.GoldMapper;
import com.kyoobiq.reducers.GoldReducer;

public class MainMapRed {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
		Configuration conf = new Configuration();
		//GenericOptionsParser p = new GenericOptionsParser(conf,args);
//		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
		Job job = new Job(conf,"Gold Map Red");
		job.setJarByClass(MainMapRed.class);
		job.setMapperClass(GoldMapper.class);
		job.setReducerClass(GoldReducer.class);
		job.setOutputKeyClass(Text.class);
		//job.setInputFormatClass(cls)
		job.setOutputValueClass(IntWritable.class);
		
		//change textinput and output format maybe
		//set combiner class maybe
		
		System.out.printf("Started: arg1:%s, arg2:%s\n",args[0],args[1]);
		//change this to other args
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		System.exit(job.waitForCompletion(true) ? 0 : 1);		
		
	}

}
