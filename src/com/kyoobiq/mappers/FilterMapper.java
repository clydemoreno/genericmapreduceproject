package com.kyoobiq.mappers;
import java.io.IOException; import java.util.StringTokenizer; import java.util.Map;
import java.util.HashMap;
import org.apache.hadoop.conf.Configuration; import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable; import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat; import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class FilterMapper extends Mapper<Object, Text, NullWritable, Text> {
	private String mapRegex = null;
	public void setup(Context context) throws IOException, InterruptedException{
		mapRegex = context.getConfiguration().get("mapregex");
	}
	public void map(Object key, Text value, Context context ) throws IOException, InterruptedException{
		if(value.toString().matches(mapRegex)){
			context.write(NullWritable.get(), value);
		}
	}
}
