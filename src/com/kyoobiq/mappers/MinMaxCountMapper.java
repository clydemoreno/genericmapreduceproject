package com.kyoobiq.mappers;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;


import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import com.kyoobiq.manager.DailyTradeTransactionManager;
import com.kyoobiq.model.Account;
import com.kyoobiq.model.DailyTradeParameter;
import com.kyoobiq.model.DailyTradeTransaction;
import com.kyoobiq.model.MatrixModel;
import com.kyoobiq.model.MinMaxCountTuple;
import com.kyoobiq.utility.AppProperties;
public class MinMaxCountMapper extends MapReduceBase implements Mapper<Object, Text, Text, DailyTradeTransaction> {
	//out output key and value Writables
	private Text uniqueId = new Text();
	private MinMaxCountTuple outTuple = new MinMaxCountTuple();
	//our output variable
	private MinMaxCountTuple result = null;
	private DailyTradeParameter param = new DailyTradeParameter();
	
	//This object will format the creationn date string into a date object
	private final static SimpleDateFormat frmt = new SimpleDateFormat("yyyy.MM.dd HH:mm");
	private final static SimpleDateFormat frmt2 = new SimpleDateFormat("yyyy.MM.dd");

	Path s3PropertiesFilePath;
	Properties prop = new Properties();
	@Override

	
	public void configure(JobConf job) {
		DailyTradeParameter param = getParam();
//		String s = job.get("asianStartHour");
//		System.out.println(s);
	    param.asianStartHour = Integer.parseInt(job.get("asianStartHour"));
	    param.tradeStartHour = Integer.parseInt(job.get("tradeStartHour"));
	    param.account.balance  = Float.parseFloat(job.get("accountBalance"));
	    param.account.averageStopLoss = Float.parseFloat(job.get("accountAverageStopLoss"));
	    param.account.riskPercentage = Float.parseFloat(job.get("accountRiskPercentage"));
	    try {
			param.dayLightStartDate = frmt2.parse(job.get("daylightSavingTimeStart"));
		    param.dayLigthEndDate = frmt2.parse(job.get("daylightSavingTimeEnd"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
		DailyTradeTransactionManager.process(param);
	}

	@Override
	public void map(Object key, Text value,
			OutputCollector<Text, DailyTradeTransaction> result, Reporter reporter)
			throws IOException {
		String line = value.toString();
    	String cvsSplitBy = ",";
		String[] trades = line.split(cvsSplitBy);
		
		if(trades.length == 7){
			SimpleDateFormat frmt = new SimpleDateFormat("yyyy.MM.dd");
			
			try {
				Date date = frmt.parse(trades[0]);
				uniqueId.set(trades[0]);
				
				DailyTradeTransaction outTuple = new DailyTradeTransaction();
				//MinMaxCountTuple outTuple = new MinMaxCountTuple();
				String dateString = String.format("%s %s",trades[0],trades[1]);
				outTuple.setTransactionDate(MinMaxCountTuple.frmt.parse(dateString));
//				int hours = outTuple.getTransactionDate().getHours();
//				int asianHour = getParam().asianStartHour;
				
				
				//trades before asian session, just don't include it.  We just need to get the hightest from 
				//asian session all the way to start of trading session.
				if(checkForSavingsDaylightOffset(outTuple, getParam())) return;
//				if(outTuple.getTransactionDate().getHours() < getParam().asianStartHour){
//					return;
//				}
				
//				outTuple.setCount(1.0f);
				outTuple.setMaxPrice(Double.parseDouble(trades[3]));
				outTuple.setMinPrice(Double.parseDouble(trades[4]));
//				outTuple.setAverage(Double.parseDouble(trades[6]));
				outTuple.setOpen(Double.parseDouble(trades[2]));
				outTuple.setHigh(Double.parseDouble(trades[3]));
				outTuple.setLow(Double.parseDouble(trades[4]));
				outTuple.setClose(Double.parseDouble(trades[5]));
				//outTuple = new MinMaxCountTuple();
				//uniqueId = new Text("2007.02.05");
				outTuple.setTransactionDate(MinMaxCountTuple.frmt.parse(dateString));
//				outTuple.setMax(outTuple.getTransactionDate());
//				outTuple.setMin(outTuple.getTransactionDate());
				
				result.collect(uniqueId, outTuple);
				//context.write(uniqueId, outTuple);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
	}
	private boolean checkForSavingsDaylightOffset(DailyTradeTransaction outTuple2,
			DailyTradeParameter param2) {
		return outTuple2.getTransactionDate().getHours() < param2.getDaylightAsianHour(outTuple2.getTransactionDate());
//		getParam().
		//if(outTuple.getTransactionDate().com)
		
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(MinMaxCountTuple result) {
		this.result = result;
	}
	/**
	 * @return the result
	 */
	public MinMaxCountTuple getResult() {
		if (result == null) result = new MinMaxCountTuple();
		return result;
	}
	/**
	 * @param param the param to set
	 */
	public void setParam(DailyTradeParameter param) {
		this.param = param;
	}
	/**
	 * @return the param
	 */
	public DailyTradeParameter getParam() {
		if (param == null) param = new DailyTradeParameter();
		return param;
	}
	
//	public void map(Object key, Text value, Context context) throws IOException, InterruptedException{
//	String line = value.toString();
//	String cvsSplitBy = ",";
//	String[] trades = line.split(cvsSplitBy);
//	if(trades.length == 7){
//		SimpleDateFormat frmt = new SimpleDateFormat("yyyy.MM.dd");
//		try {
//			Date date = frmt.parse(trades[0]);
//			uniqueId.set(trades[0]);
//			outTuple.setMax(date);
//			outTuple.setMin(date);
//			outTuple.setCount(1.0f);
//			outTuple.setMaxPrice(Float.parseFloat(trades[2]));
//			outTuple.setMinPrice(Float.parseFloat(trades[2]));
//			outTuple.setAverage(Float.parseFloat(trades[6]));
//			context.write(uniqueId, outTuple);
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//	}
//	
//}
	
}
