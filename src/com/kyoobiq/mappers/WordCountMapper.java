package com.kyoobiq.mappers;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class WordCountMapper extends Mapper<Object, Text, Text, IntWritable> {
	private final static IntWritable one = new IntWritable(1);
	private Text word = new Text();
	
	public void map(Object key, Text value, Context context) throws IOException, InterruptedException{
		String line = value.toString();
    	String cvsSplitBy = ",";
		String[] trades = line.split(cvsSplitBy);
		System.out.println(String.format("Date: %s Time: %s ",trades[0], trades[1]));
		if(trades.length == 7){
			word.set(trades[0]);
			context.write(word, one);
		}
	}
}
