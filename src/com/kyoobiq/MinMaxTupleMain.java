package com.kyoobiq;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.lib.IdentityReducer;
import org.apache.hadoop.mapreduce.Job;

import com.kyoobiq.mappers.MinMaxCountMapper;
import com.kyoobiq.mappers.WordCountMapper;
import com.kyoobiq.model.DailyTradeTransaction;
import com.kyoobiq.model.MinMaxCountTuple;
import com.kyoobiq.reducers.IntSumReducer;
import com.kyoobiq.reducers.MinMaxCombiner;
import com.kyoobiq.reducers.MinMaxReducer;
import com.kyoobiq.utility.AppProperties;

public class MinMaxTupleMain {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws InterruptedException 
	 * @throws URISyntaxException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException, URISyntaxException {
		System.out.println(String.format("Start Date: %s\n",new Date()));
	
		Configuration config = new Configuration();
		populateConfig(config);
		
		JobConf conf = new JobConf(config,MinMaxTupleMain.class);
//		String s3PropertiesFilePath = "http://com.kyoobiq.inputfiles.s3.amazonaws.com/s3InputFiles/App.properties";
		//String s3PropertiesFilePath = "/Users/clydemoreno/Documents/workspace/GenericMapRedProject/App.properties ";
		//DistributedCache.addCacheFile(new URI(s3PropertiesFilePath), conf);
        conf.setJobName("MinMaxMain");
        conf.setOutputKeyClass(Text.class);
        conf.setOutputValueClass(DailyTradeTransaction.class);
        
        conf.setMapperClass(MinMaxCountMapper.class);
        conf.setCombinerClass(MinMaxCombiner.class);
        conf.setReducerClass(IdentityReducer.class);
        conf.setCompressMapOutput(true);
        
//        conf.setMapOutputKeyClass(Text.class);
//        conf.setMapOutputValueClass(DailyTradeTransaction.class);
        
        
        FileInputFormat.setInputPaths(conf, new Path(args[0]));
        FileOutputFormat.setOutputPath(conf, new Path(args[1]));
        JobClient.runJob(conf);		
		
        //find out if conf will be passed to the mappers and reducers
		
		
//		Configuration conf = new Configuration();
//		Job job = new Job(conf,"Min Max Tuple Main");
//		job.setJarByClass(MinMaxTupleMain.class);
//		job.setMapperClass(MinMaxCountMapper.class);
//		job.setCombinerClass(MinMaxCountReducer.class);
//		job.setReducerClass(MinMaxCountReducer.class);
//		job.setOutputKeyClass(Text.class);
//		job.setOutputValueClass(MinMaxCountTuple.class);
//		FileInputFormat.addInputPath(job, new Path(args[0]));
//		FileOutputFormat.setOutputPath(job, new Path(args[1]));
//		System.out.println(String.format("End Date: %s\n",new Date()));
		
//		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
	public static void populateConfig(Configuration config) {
		AppProperties appProperties = AppProperties.getInstance();
        appProperties.load();
		
		
		config.set("tradeStartHour", appProperties.getProperty("tradeStartHour"));
		config.set("tradeEndHour", appProperties.getProperty("tradeEndHour"));
		config.set("asianStartHour", appProperties.getProperty("asianStartHour"));
		config.set("accountBalance", appProperties.getProperty("accountBalance"));
		config.set("accountAverageStopLoss", appProperties.getProperty("accountAverageStopLoss"));
		config.set("accountRiskPercentage", appProperties.getProperty("accountRiskPercentage"));
		config.set("daylightSavingTimeStart", appProperties.getProperty("daylightSavingTimeStart"));
		config.set("daylightSavingTimeEnd", appProperties.getProperty("daylightSavingTimeEnd"));
		
	}


}
