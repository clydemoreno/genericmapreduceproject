package com.kyoobiq.utility;

import com.kyoobiq.model.DailyTradeTransaction;


public class Calculator {
	public static double getProfitTarget(int type, double d, double e, float percentage)
	{
		if(type==1)	
			return d+((d-e)*percentage);
		else	
			return e-((d-e)*percentage);
	}
	public static double getStopLoss(double d, double e)
	{
		return d-((d-e)/2);
	}
	public static double calculateProfit(DailyTradeTransaction buyDailyTradeTransaction) {
		return ((buyDailyTradeTransaction.stopLoss-buyDailyTradeTransaction.entry))*buyDailyTradeTransaction.lotSize;
	}	
}
