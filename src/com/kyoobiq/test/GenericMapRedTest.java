package com.kyoobiq.test;


import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.jobcontrol.Job;
import org.apache.hadoop.mrunit.MapDriver;
import org.apache.hadoop.mrunit.MapReduceDriver;
import org.apache.hadoop.mrunit.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;

import com.kyoobiq.MinMaxTupleMain;
import com.kyoobiq.manager.ExtractTransformLoadManager;
import com.kyoobiq.mappers.MinMaxCountMapper;
import com.kyoobiq.model.DailyTradeTransaction;
import com.kyoobiq.model.MinMaxCountTuple;
import com.kyoobiq.model.Trade;
import com.kyoobiq.reducers.MinMaxCombiner;
import com.kyoobiq.reducers.MinMaxReducer;
import com.kyoobiq.utility.AppProperties;
public class GenericMapRedTest {

//	MapDriver<Object, Text, Text, MinMaxCountTuple> mapDriver;
//	ReduceDriver<Text,MinMaxCountTuple, Text, MinMaxCountTuple> reduceDriver;
	MapReduceDriver<Object, Text, Text, DailyTradeTransaction,Text, DailyTradeTransaction> dm;

	@Before
	public void setUp() throws Exception {
		
		dm = new MapReduceDriver<Object, Text, Text, DailyTradeTransaction,Text, DailyTradeTransaction>();
		//create mapper and reducer objects
		Configuration config = new Configuration();
		MinMaxTupleMain.populateConfig(config);
		
		MinMaxCountMapper mapper = new MinMaxCountMapper();
		Reducer combiner  = new MinMaxCombiner();
		Reducer reducer = new MinMaxReducer();
		//JobConf conf = new JobConf(config);
		//mapper.configure(conf);
		//reducer.configure(conf);
		dm.setConfiguration(config);
		
		dm.withMapper(mapper);
		dm.withCombiner(combiner);
		dm.withReducer(reducer);
		
	}
//	public void testMapper() throws IOException{
//		
//		Text input = new Text("2007.02.05,22:46,633.70000,633.70000,633.70000,633.70000,1");
//		mapDriver.setInput(new LongWritable(1), input);
//		
//		mapDriver.withOutput(new Text("2007.02.05"), new MinMaxCountTuple());
//		
////		mapDriver.resetOutput();
////		input = new Text("2007.02.05,22:46,633.50000,633.70000,633.70000,633.70000,2");
////		mapDriver.setInput(new LongWritable(1), input);
//		mapDriver.run();
//
////		2007.05.17,00:01,633.55000,633.55000,633.55000,633.55000,3
////		2007.05.17,03:31,663.20000,663.20000,663.15000,663.15000,3
////		2007.05.17,03:32,663.15000,663.20000,663.15000,663.20000,4
////		2007.05.17,03:33,663.05000,663.05000,663.05000,663.05000,2
////		2007.05.17,03:34,663.20000,663.25000,663.20000,663.25000,3
////		2007.05.17,03:35,663.20000,663.20000,663.15000,663.20000,6
////		2007.05.17,03:36,662.95000,663.05000,662.95000,663.00000,9
////		2007.05.17,03:37,663.05000,663.05000,663.05000,663.05000,1
////		2007.05.17,03:38,663.20000,663.20000,663.15000,663.15000,2
//	}

	@Test
	public void testMapReduceFromFile() throws IOException{
		
		ExtractTransformLoadManager m = new ExtractTransformLoadManager();
		m.extract(dm);
//		for(String k : list.keySet()){
//			System.out.println(list.get(k));
//			Trade t = list.get(k);
//			Text entry = new Text();
//			
//			entry.set(String.format("%s,%s,%s,%s,%s", t.id,t.open,t.high, t.low, t.close));
//			//dm.withInput(key, val)
//		}
		
//		Text[] s = new Text[6];
//		Text input = new Text("2007.02.05,02:36,633.70020,633.70000,633.70000,633.70000,1");
//		s[0] = input;
//		input = new Text("2007.02.05,03:37,633.70081,633.70090,633.70040,633.70000,1");
//		s[1] = input;
//		input = new Text("2007.02.05,04:38,633.70082,633.70092,633.70030,633.70000,1");
//		s[2] = input;
//		input = new Text("2007.02.05,05:39,633.70083,633.70098,633.70020,633.70000,3");//high is 98
//		s[3] = input;
//		input = new Text("2007.02.05,06:01,633.70080,633.70090,633.70019,633.70000,2");//low is 19
//		//sell entry
//		input = new Text("2007.02.05,14:10,633.70002,633.70029,633.70001,633.70020,1");//
//		s[4] = input;
//		input = new Text("2007.02.05,14:35,633.70006,633.70059,633.70004,633.70007,1");//
//		s[5] = input;
//
//		for (int i = 0; i < s.length; i++) {
//			dm.withInput(new LongWritable(i + 1), s[i]);
//			
//		}
//		dm.withCombiner(new MinMaxCountReducer());
//		MinMaxCountReducer reducer = new MinMaxCountReducer();
//		JobConf job = new JobConf(config);
//		mapper.configure(job);
//		reducer.configure(job);
//		dm.withReducer(reducer);
		
		//(2007.02.05, 633.7	633.71	5.0	1.0)
//		DailyTradeTransaction t = new DailyTradeTransaction();
//		t.setMaxPrice((double)633.70098);
//		t.setMinPrice((double)633.70019);
//		t.setCount((double)4.00000);
//		t.setAverage((double)1.00000);
//		DecimalFormat df = new DecimalFormat("0.00000");
//		
//		String ss = String.format("%s\t%s\t%s\t%s\t%s", "2007.02.05", df.format(t.getMinPrice()),df.format(t.getMaxPrice()),df.format( t.getCount()), df.format(t.getAverage()));
		//ss = "633.00001	5.00000	1.00000	633.71000";
//		dm.withOutputFromString(ss);
		List<Pair<Text, DailyTradeTransaction>> output = dm.run();
		for(Pair<Text,DailyTradeTransaction> item : output){
			System.out.println(item);
//			if(item.compareTo(new Pair(new Text("2007.02.05"),t)) > 0){
//				System.out.println(item);
//				
//			}
//			Assert.assertEquals(item.compareTo(new Pair(new Text("2007.02.05"),t)), 1);
		}
	
	}
	//@Test
	@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })

	public void testBuyMapReduce() throws IOException{
		
		Text[] s = new Text[14];
		Text input = new Text("2007.02.05,02:36,633.70020,633.70000,633.70000,633.70000,1");
		s[0] = input;
		input = new Text("2007.02.05,03:37,633.70081,633.70090,633.70040,633.70000,1");
		s[1] = input;
		input = new Text("2007.02.05,04:38,633.70082,633.70092,633.70030,633.70000,1");
		s[2] = input;
		input = new Text("2007.02.05,05:39,633.70083,633.70098,633.70020,633.70000,3");//high is 98
		s[3] = input;
		input = new Text("2007.02.05,06:01,633.70080,633.70090,633.70019,633.70000,2");//low is 19
		s[4] = input;
		input = new Text("2007.02.05,12:01,633.70068,633.70070,633.70069,633.70071,1");//(start of asian session) (triggers a buy )
		s[5] = input;
		input = new Text("2007.02.05,13:40,633.70099,633.70102,633.70067,633.70098,1");//
		s[6] = input;

		//buy profit targets
		input = new Text("2007.02.05,13:45,633.70145,633.70150,633.70144,633.70146,1");//
		s[7] = input;
		input = new Text("2007.02.05,13:50,633.70146,633.70162,633.70159,633.70176,1");//
		s[8] = input;
		input = new Text("2007.02.05,13:55,633.70176,633.70179,633.70170,633.70175,1");//
		s[9] = input;

//sell entry		
		input = new Text("2007.02.07,03:37,633.70081,633.70090,633.70040,633.70000,1");
		s[10] = input;
		input = new Text("2007.02.07,04:38,633.70082,633.70092,633.70030,633.70000,1");
		s[11] = input;
		input = new Text("2007.02.07,05:39,633.70083,633.70098,633.70020,633.70000,3");//high is 98
		s[12] = input;
		input = new Text("2007.02.07,06:01,633.70080,633.70090,633.70019,633.70000,2");//low is 19
		s[13] = input;
		
		
		//sell entry
		input = new Text("2007.02.07,14:10,633.70019,633.70029,633.70018,633.70020,1");//
		s[10] = input;
		input = new Text("2007.02.07,14:35,633.70006,633.70029,633.70004,633.70007,1");//
		s[11] = input;
		
		
		
		//		MinMaxCountMapper mapper = new MinMaxCountMapper();
//		dm.withMapper(mapper);
		
		
		
		for (int i = 0; i < s.length; i++) {
			dm.withInput(new LongWritable(i + 1), s[i]);
			
		}
//		dm.withCombiner(new MinMaxCountReducer());
//		MinMaxCountReducer reducer = new MinMaxCountReducer();
//		JobConf job = new JobConf(config);
//		mapper.configure(job);
//		reducer.configure(job);
//		dm.withReducer(reducer);
		
		//(2007.02.05, 633.7	633.71	5.0	1.0)
		MinMaxCountTuple t = new MinMaxCountTuple();
		t.setMaxPrice((double)633.70098);
		t.setMinPrice((double)633.70019);
		t.setCount((double)4.00000);
		t.setAverage((double)1.00000);
		DecimalFormat df = new DecimalFormat("0.00000");
		
		String ss = String.format("%s\t%s\t%s\t%s\t%s", "2007.02.05", df.format(t.getMinPrice()),df.format(t.getMaxPrice()),df.format( t.getCount()), df.format(t.getAverage()));
		//ss = "633.00001	5.00000	1.00000	633.71000";
		dm.withOutputFromString(ss);
		List<Pair<Text, DailyTradeTransaction>> output = dm.run();
		for(Pair<Text,DailyTradeTransaction> item : output){
			System.out.println(item);
//			if(item.compareTo(new Pair(new Text("2007.02.05"),t)) > 0){
//				System.out.println(item);
//				
//			}
//			Assert.assertEquals(item.compareTo(new Pair(new Text("2007.02.05"),t)), 1);
		}
		
	}
	private void populateConfigToBeDeleted(Configuration config) {
		AppProperties appProperties = AppProperties.getInstance();
        appProperties.load();
		
		
		config.set("tradeStartHour", appProperties.getProperty("tradeStartHour"));
		config.set("asianStartHour", appProperties.getProperty("asianStartHour"));
		config.set("accountBalance", appProperties.getProperty("accountBalance"));
		config.set("accountAverageStopLoss", appProperties.getProperty("accountAverageStopLoss"));
		config.set("accountRiskPercentage", appProperties.getProperty("accountRiskPercentage"));
		config.set("daylightSavingTimeStart", appProperties.getProperty("daylightSavingTimeStart"));
		config.set("daylightSavingTimeEnd", appProperties.getProperty("daylightSavingTimeEnd"));
		
	}
	

}
