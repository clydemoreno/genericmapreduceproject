package com.kyoobiq.reducers;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

import com.kyoobiq.manager.DailyTradeTransactionManager;
import com.kyoobiq.model.DailyTradeParameter;
import com.kyoobiq.model.DailyTradeTransaction;
import com.kyoobiq.model.MinMaxCountTuple;

public class MinMaxReducer extends MapReduceBase implements Reducer<Text,DailyTradeTransaction, Text, DailyTradeTransaction> {

	
	@Override
	public void reduce(Text key, Iterator<DailyTradeTransaction> values,
			OutputCollector<Text, DailyTradeTransaction> outputCollector, Reporter reporter)
			throws IOException {
//		MinMaxCountTuple result = new MinMaxCountTuple();
//		result.setMin(new Date());
//		result.setMax(new Date());
//		result.setCount(0);
//		result.setMaxPrice((float) 0.0);
//		result.setMinPrice((float) 0.0);
//		double sum = 0;
//		double count = 0;
		while(values.hasNext()){
			DailyTradeTransaction val = (DailyTradeTransaction)values.next();
			outputCollector.collect(key, val);
		}	
	}

}
