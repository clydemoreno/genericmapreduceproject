package com.kyoobiq.reducers;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

import com.kyoobiq.manager.DailyTradeTransactionManager;
import com.kyoobiq.model.DailyTradeParameter;
import com.kyoobiq.model.MinMaxCountTuple;

public class NoReducer extends MapReduceBase implements Reducer<Text,MinMaxCountTuple, Text, MinMaxCountTuple> {

	private MinMaxCountTuple result = null;
	private DailyTradeParameter param = new DailyTradeParameter();

	public void configure(JobConf job) {
		DailyTradeParameter param = getParam();
	    param.asianStartHour = Integer.parseInt(job.get("asianStartHour"));
	    param.tradeStartHour = Integer.parseInt(job.get("tradeStartHour"));
	    param.account.balance  = Float.parseFloat(job.get("accountBalance"));
	    param.account.averageStopLoss = Float.parseFloat(job.get("accountAverageStopLoss"));
	    param.account.riskPercentage = Float.parseFloat(job.get("accountRiskPercentage"));
		DailyTradeTransactionManager.process(param);
	}
	/**
	 * @return the param
	 */
	public DailyTradeParameter getParam() {
		if (param == null) param = new DailyTradeParameter();
		return param;
	}
	

	
	@Override
	public void reduce(Text key, Iterator<MinMaxCountTuple> values,
			OutputCollector<Text, MinMaxCountTuple> outputCollector, Reporter reporter)
			throws IOException {
		@SuppressWarnings("unused")
		MinMaxCountTuple result = new MinMaxCountTuple();
		result.setMin(new Date());
		result.setMax(new Date());
		result.setCount(0);
		result.setMaxPrice((float) 0.0);
		result.setMinPrice((float) 0.0);
		double sum = 0;
		double count = 0;
		while(values.hasNext()){
			MinMaxCountTuple val = (MinMaxCountTuple)values.next();
			sum += val.getAverage();
			count += val.getCount();
		}	
		result.setAverage(sum);
		result.setCount(count);
		outputCollector.collect(key, result);
	}

}
