package com.kyoobiq.reducers;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration; 
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable; 
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat; 
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class GoldReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
	public void reduce(Text key, Iterable<IntWritable> values, OutputCollector<Text, IntWritable> context) throws IOException, InterruptedException {
        // Write the user's id with a null value
		int count = 0;
		for(IntWritable t:values){
			count += t.get();
		}
//		Text txt = new Text( String.valueOf(count));
		IntWritable w = new IntWritable(count);
		context.collect(key, w);
	}
}
