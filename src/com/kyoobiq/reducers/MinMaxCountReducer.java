package com.kyoobiq.reducers;
import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

import com.kyoobiq.model.MinMaxCountTuple;
public class MinMaxCountReducer extends MapReduceBase implements Reducer<Text,MinMaxCountTuple, Text, MinMaxCountTuple>{
	
	//our output variable
	private MinMaxCountTuple result = new MinMaxCountTuple();

	@Override
	public void reduce(Text key, Iterator<MinMaxCountTuple> values,
			OutputCollector<Text, MinMaxCountTuple> outputCollector, Reporter report)
			throws IOException {
		//initialize our result
		MinMaxCountTuple result = new MinMaxCountTuple();
		result.setMin(null);
		result.setMax(null);
		result.setCount(0);
		result.setMaxPrice((float) 0.0);
		result.setMinPrice((float) 0.0);
		float sum = 0;
		float count = 0;
		//Iterate through all input values for this key
		while(values.hasNext()){
			MinMaxCountTuple val = (MinMaxCountTuple)values.next();
			if(result.getMin() == null || val.getMin().compareTo(result.getMin()) < 0){
				result.setMin(val.getMin());
			}
			if(result.getMax() == null || val.getMax().compareTo(result.getMax()) > 0){
				result.setMax(val.getMax());
			}
			if(result.getMinPrice() == (float) 0.0 || val.getMinPrice() < (result.getMinPrice()) ){
				result.setMinPrice(val.getMinPrice());
			}
			if(result.getMaxPrice() == (float) 0.0 || val.getMaxPrice() > (result.getMaxPrice()) ){
				result.setMaxPrice(val.getMaxPrice());
			}
			sum += (val.getCount() * val.getAverage());
			count += val.getCount();

		}
//		for(MinMaxCountTuple val : values){
//			//if the value's min isless than result's min
//			//set result's min to value's
//			if(result.getMin() == null || val.getMin().compareTo(result.getMin()) < 0){
//				result.setMin(val.getMin());
//			}
//			if(result.getMax() == null || val.getMax().compareTo(result.getMax()) > 0){
//				result.setMax(val.getMax());
//			}
//			if(result.getMinPrice() == (float) 0.0 || val.getMinPrice() < (result.getMinPrice()) ){
//				result.setMinPrice(val.getMinPrice());
//			}
//			if(result.getMaxPrice() == (float) 0.0 || val.getMaxPrice() > (result.getMaxPrice()) ){
//				result.setMaxPrice(val.getMaxPrice());
//			}
//			sum += (val.getCount() * val.getAverage());
//			count += val.getCount();
//		}
		//set our final values
		result.setCount(count);
		result.setAverage(sum / count);
		//result = new MinMaxCountTuple();
		//key = new Text("2007.02.05");
		outputCollector.collect(key, result);
		//context.write(key, result);
		
	}
	
//	public void reduce(Text key, Iterable<MinMaxCountTuple> values, Context context) throws IOException, InterruptedException{
//		//initialize our result
//		result.setMin(null);
//		result.setMax(null);
//		result.setCount(0);
//		result.setMaxPrice((float) 0.0);
//		result.setMinPrice((float) 0.0);
//		float sum = 0;
//		float count = 0;
//		//Iterate through all input values for this key
//		for(MinMaxCountTuple val : values){
//			//if the value's min isless than result's min
//			//set result's min to value's
//			if(result.getMin() == null || val.getMin().compareTo(result.getMin()) < 0){
//				result.setMin(val.getMin());
//			}
//			if(result.getMax() == null || val.getMax().compareTo(result.getMax()) > 0){
//				result.setMax(val.getMax());
//			}
//			if(result.getMinPrice() == (float) 0.0 || val.getMinPrice() < (result.getMinPrice()) ){
//				result.setMinPrice(val.getMinPrice());
//			}
//			if(result.getMaxPrice() == (float) 0.0 || val.getMaxPrice() > (result.getMaxPrice()) ){
//				result.setMaxPrice(val.getMaxPrice());
//			}
//			sum += (val.getCount() * val.getAverage());
//			count += val.getCount();
//		}
//		//set our final values
//		result.setCount(count);
//		result.setAverage(sum / count);
//		context.write(key, result);
//	}
}
