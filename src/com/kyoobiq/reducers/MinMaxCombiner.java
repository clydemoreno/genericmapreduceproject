package com.kyoobiq.reducers;
import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

import com.kyoobiq.manager.DailyTradeTransactionManager;
import com.kyoobiq.manager.MinMaxReduceManager;
import com.kyoobiq.model.Account;
import com.kyoobiq.model.DailyTradeParameter;
import com.kyoobiq.model.DailyTradeTransaction;
import com.kyoobiq.model.MinMaxCountTuple;
public class MinMaxCombiner extends MapReduceBase implements Reducer<Text,DailyTradeTransaction, Text, DailyTradeTransaction>{
	
	//our output variable
	private MinMaxCountTuple result = null;
	private DailyTradeParameter param = new DailyTradeParameter();

	public void configure(JobConf job) {
		DailyTradeParameter param = getParam();
	    param.asianStartHour = Integer.parseInt(job.get("asianStartHour"));
	    param.tradeStartHour = Integer.parseInt(job.get("tradeStartHour"));
	    param.tradeEndHour = Integer.parseInt(job.get("tradeEndHour"));
	    param.account.balance  = Float.parseFloat(job.get("accountBalance"));
	    param.account.averageStopLoss = Float.parseFloat(job.get("accountAverageStopLoss"));
	    param.account.riskPercentage = Float.parseFloat(job.get("accountRiskPercentage"));
		DailyTradeTransactionManager.process(param);
	}
	@Override
	public void reduce(Text key, Iterator<DailyTradeTransaction> values,
			OutputCollector<Text, DailyTradeTransaction> outputCollector, Reporter report)
			throws IOException {
		MinMaxReduceManager manager = new MinMaxReduceManager();
		try {
			manager.processReduce(key, values, outputCollector,param);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
//	private void processReduce(Text key, Iterator<MinMaxCountTuple> values,
//			OutputCollector<Text, MinMaxCountTuple> outputCollector)
//			throws IOException {
//		//initialize our result
//		MinMaxCountTuple result = new MinMaxCountTuple();
//		result.setMin(null);
//		result.setMax(null);
//		result.setCount(0);
//		result.setMaxPrice((float) 0.0);
//		result.setMinPrice((float) 0.0);
//		float sum = 0;
//		float count = 0;
//		//Iterate through all input values for this key
//		while(values.hasNext()){
//			MinMaxCountTuple val = (MinMaxCountTuple)values.next();
//			if(result.getMin() == null || val.getMin().compareTo(result.getMin()) < 0){
//				result.setMin(val.getMin());
//			}
//			if(result.getMax() == null || val.getMax().compareTo(result.getMax()) > 0){
//				result.setMax(val.getMax());
//			}
//			if(result.getMinPrice() == (float) 0.0 || val.getMinPrice() < (result.getMinPrice()) ){
//				result.setMinPrice(val.getMinPrice());
//			}
//			if(result.getMaxPrice() == (float) 0.0 || val.getMaxPrice() > (result.getMaxPrice()) ){
//				result.setMaxPrice(val.getMaxPrice());
//			}
//			sum += (val.getCount() * val.getAverage());
//			count += val.getCount();
//
//		}
//		//set our final values
//		result.setCount(count);
//		result.setAverage(sum / count);
//		
//		outputCollector.collect(key, result);
//	}
	/**
	 * @param result the result to set
	 */
	public void setResult(MinMaxCountTuple result) {
		this.result = result;
	}
	/**
	 * @return the result
	 */
	public MinMaxCountTuple getResult() {
		if (result == null) result = new MinMaxCountTuple();
		return result;
	}
	/**
	 * @param param the param to set
	 */
	public void setParam(DailyTradeParameter param) {
		this.param = param;
	}
	/**
	 * @return the param
	 */
	public DailyTradeParameter getParam() {
		if (param == null) param = new DailyTradeParameter();
		return param;
	}
	
//	public void reduce(Text key, Iterable<MinMaxCountTuple> values, Context context) throws IOException, InterruptedException{
//		//initialize our result
//		result.setMin(null);
//		result.setMax(null);
//		result.setCount(0);
//		result.setMaxPrice((float) 0.0);
//		result.setMinPrice((float) 0.0);
//		float sum = 0;
//		float count = 0;
//		//Iterate through all input values for this key
//		for(MinMaxCountTuple val : values){
//			//if the value's min isless than result's min
//			//set result's min to value's
//			if(result.getMin() == null || val.getMin().compareTo(result.getMin()) < 0){
//				result.setMin(val.getMin());
//			}
//			if(result.getMax() == null || val.getMax().compareTo(result.getMax()) > 0){
//				result.setMax(val.getMax());
//			}
//			if(result.getMinPrice() == (float) 0.0 || val.getMinPrice() < (result.getMinPrice()) ){
//				result.setMinPrice(val.getMinPrice());
//			}
//			if(result.getMaxPrice() == (float) 0.0 || val.getMaxPrice() > (result.getMaxPrice()) ){
//				result.setMaxPrice(val.getMaxPrice());
//			}
//			sum += (val.getCount() * val.getAverage());
//			count += val.getCount();
//		}
//		//set our final values
//		result.setCount(count);
//		result.setAverage(sum / count);
//		context.write(key, result);
//	}
}
