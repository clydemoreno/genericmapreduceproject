package com.kyoobiq.model;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Date;

import org.apache.hadoop.io.Writable;

public class DailyTradeTransaction implements Writable {

	
	public double entry;
//	public String entryTime;
//	public String exitTime;
	public Date entryTime = new Date();
	public Date exitTime = new Date();
	public double stopLoss;
	public double profitTarget;
	public float profitTartgetPercentage;
	public float lotSize;
	public float lotSizePercentage;
	public int type;
	public Boolean isEntryPriceGotHit = false;
	public Boolean isStopLossGotHit = false;
	public Boolean isProfitTargetHit = false;
	public Boolean active = true;
	public double profit;
	public int tradeNumber;
	public String id;
	
	public double getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(double maxPrice) {
		this.maxPrice = maxPrice;
	}
	public double getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(double minPrice) {
		this.minPrice = minPrice;
	}
	public double getOpen() {
		return open;
	}
	public void setOpen(double open) {
		this.open = open;
	}
	public double getHigh() {
		return high;
	}
	public void setHigh(double high) {
		this.high = high;
	}
	public double getLow() {
		return low;
	}
	public void setLow(double low) {
		this.low = low;
	}
	public double getClose() {
		return close;
	}
	public void setClose(double close) {
		this.close = close;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	private double maxPrice;
	private double minPrice;
	private double open;
	private double high;
	private double low;
	private double close;
	private Date transactionDate = new Date();	
	
	@Override
	public String toString() {
		String s = "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s";
		return String.format(s,entry,MinMaxCountTuple.frmt.format(entryTime),MinMaxCountTuple.frmt.format(exitTime),stopLoss,profitTarget,profitTartgetPercentage
				,lotSize,lotSizePercentage,profit,tradeNumber, type, MinMaxCountTuple.frmt.format(transactionDate));
//		return "DailyTradeTransaction [entry=" + entry + ", entryTime="
//				+ entryTime + ", exitTime=" + exitTime + ", stopLoss="
//				+ stopLoss + ", profitTarget=" + profitTarget
//				+ ", profitTartgetPercentage=" + profitTartgetPercentage
//				+ ", lotSize=" + lotSize + ", lotSizePercentage="
//				+ lotSizePercentage + ", type=" + type
//				+ ", isEntryPriceGotHit=" + isEntryPriceGotHit
//				+ ", isStopLossGotHit=" + isStopLossGotHit
//				+ ", isProfitTargetHit=" + isProfitTargetHit + ", active="
//				+ active + ", profit=" + profit + ", tradeNumber="
//				+ tradeNumber + ", id=" + id + "]";
	}
	@Override
	public void readFields(DataInput in) throws IOException {
		
		entry = in.readDouble();
		entryTime = new Date(in.readLong());
		exitTime = new Date(in.readLong());
//		entryTime = in.readLine();
//		exitTime = in.readLine();
		
		stopLoss = in.readDouble();
		profitTarget = in.readDouble();
		profitTartgetPercentage = in.readFloat();
		lotSize = in.readFloat();
		lotSizePercentage = in.readFloat();
		type = in.readInt();
		isEntryPriceGotHit = in.readBoolean();
		isStopLossGotHit = in.readBoolean();
		isProfitTargetHit = in.readBoolean();
		active = in.readBoolean();
		profit = in.readDouble();
		tradeNumber = in.readInt();
		//id = in.readLine();
		maxPrice = in.readDouble();
		minPrice = in.readDouble();
		open = in.readDouble();
		high = in.readDouble();
		low = in.readDouble();
		close = in.readDouble();
		transactionDate = new Date(in.readLong());

		
		
	}
	@Override
	public void write(DataOutput out) throws IOException {		
		
		out.writeDouble(entry);
		out.writeLong(entryTime.getTime());
		out.writeLong(exitTime.getTime());
//		out.writeUTF(entryTime);
//		out.writeUTF(exitTime);
		out.writeDouble(stopLoss);
		out.writeDouble(profitTarget);
		out.writeFloat(profitTartgetPercentage);
		out.writeFloat(lotSize);
		out.writeFloat(lotSizePercentage);
		out.writeInt(type);
		out.writeBoolean(isEntryPriceGotHit);
		out.writeBoolean(isStopLossGotHit);
		out.writeBoolean(isProfitTargetHit);
		out.writeBoolean(active);
		out.writeDouble(profit);
		out.writeInt(tradeNumber);
		//out.writeUTF(id);
		out.writeDouble(maxPrice);
		out.writeDouble(minPrice);
		out.writeDouble(open);
		out.writeDouble(high);
		out.writeDouble(low);
		out.writeDouble(close);
		out.writeLong(transactionDate.getTime());
		
	
		
	}
}
