package com.kyoobiq.model;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.hadoop.io.Writable;

public class MinMaxCountTuple implements Writable, Comparable<MinMaxCountTuple> {

	private Date min = new Date();
	private Date max = new Date();
	private double count = 0;
	private double maxPrice = 0.0f;
	private double minPrice = 0.0f;
	private double average;
	private double open;
	private double high;
	private double low;
	private double close;
	private Date transactionDate = new Date();
	 
	//2007.02.05	22:46
	public final static SimpleDateFormat frmt = new SimpleDateFormat("yyyy.MM.dd HH:mm");
	
	
	
	@Override
	public void readFields(DataInput in) throws IOException {
		// Read the data out in the order it is written,
		// creating new Date objects from the UNIX timestamp
		this.min = new Date(in.readLong());
		this.max = new Date(in.readLong());
		this.setCount(in.readDouble());
		this.maxPrice = in.readDouble();
		this.minPrice = in.readDouble();
		this.average = in.readDouble();
		this.setOpen(in.readDouble());
		this.setHigh(in.readDouble());
		this.setLow(in.readDouble());
		this.setClose(in.readDouble());
		this.setTransactionDate(new Date( in.readLong()));
		
	}

	@Override
	public void write(DataOutput out) throws IOException {
		// Write the data out in the order it is read, 
		// using the UNIX timestamp to represent the Date
		out.writeLong(min.getTime());
		out.writeLong(max.getTime());
		out.writeDouble(getCount());
		out.writeDouble(maxPrice);
		out.writeDouble(minPrice);
		out.writeDouble(average);
		out.writeDouble(getOpen());
		out.writeDouble(getHigh());
		out.writeDouble(getLow());
		out.writeDouble(getClose());
		out.writeLong(transactionDate.getTime());
		
		
	}

	public void setMin(Date min) {
		this.min = min;
	}

	public Date getMin() {
		return min;
	}

	public void setMax(Date max) {
		this.max = max;
	}

	public Date getMax() {
		return max;
	}

	/**
	 * @param maxPrice the maxPrice to set
	 */
	public void setMaxPrice(double maxPrice) {
		this.maxPrice = maxPrice;
	}

	/**
	 * @return the maxPrice
	 */
	public double getMaxPrice() {
		return maxPrice;
	}

	/**
	 * @param minPrice the minPrice to set
	 */
	public void setMinPrice(double minPrice) {
		this.minPrice = minPrice;
	}

	/**
	 * @return the minPrice
	 */
	public double getMinPrice() {
		return minPrice;
	}
	
	public String toString(){
		DecimalFormat df = new DecimalFormat("0.00000");
		//String ss = String.format("%s	%s	%s	%s	%s", "2007.02.05", df.format(t.getMinPrice()),df.format( t.getCount()), df.format(t.getAverage()),df.format(t.getMaxPrice()));
	
		return String.format( "%s\t%s\t%s\t%s", df.format(getMinPrice())
				, df.format(getMaxPrice())
				,df.format(getCount())
				, df.format(getAverage())
				, df.format(getOpen())
				, df.format(getHigh())
				, df.format(getLow())
				, df.format(getClose())
				, frmt.format(getTransactionDate())
				, frmt.format(getMax())
				, frmt.format(getMin())
				
		);
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(double count) {
		this.count = count;
	}

	/**
	 * @return the count
	 */
	public double getCount() {
		return count;
	}

	/**
	 * @param average the average to set
	 */
	public void setAverage(double average) {
		this.average = average;
	}

	/**
	 * @return the average
	 */
	public double getAverage() {
		return average;
	}

//	public boolean equals(MinMaxCountTuple o){
//		return true;
//	}
	public int compareTo(MinMaxCountTuple o) {
		// TODO Auto-generated method stub
		//return 0;
//		return this.equals(o) ? 1 : 0;
		
		return (
				this.maxPrice == o.maxPrice && 
				this.minPrice == o.minPrice
				) ? 1 : 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(average);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(close);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(count);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(high);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(low);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((max == null) ? 0 : max.hashCode());
		temp = Double.doubleToLongBits(maxPrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((min == null) ? 0 : min.hashCode());
		temp = Double.doubleToLongBits(minPrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(open);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((transactionDate == null) ? 0 : transactionDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MinMaxCountTuple other = (MinMaxCountTuple) obj;
		if (Double.doubleToLongBits(average) != Double
				.doubleToLongBits(other.average))
			return false;
		if (Double.doubleToLongBits(close) != Double
				.doubleToLongBits(other.close))
			return false;
		if (Double.doubleToLongBits(count) != Double
				.doubleToLongBits(other.count))
			return false;
		if (Double.doubleToLongBits(high) != Double
				.doubleToLongBits(other.high))
			return false;
		if (Double.doubleToLongBits(low) != Double.doubleToLongBits(other.low))
			return false;
		if (max == null) {
			if (other.max != null)
				return false;
		} else if (!max.equals(other.max))
			return false;
		if (Double.doubleToLongBits(maxPrice) != Double
				.doubleToLongBits(other.maxPrice))
			return false;
		if (min == null) {
			if (other.min != null)
				return false;
		} else if (!min.equals(other.min))
			return false;
		if (Double.doubleToLongBits(minPrice) != Double
				.doubleToLongBits(other.minPrice))
			return false;
		if (Double.doubleToLongBits(open) != Double
				.doubleToLongBits(other.open))
			return false;
		if (transactionDate == null) {
			if (other.transactionDate != null)
				return false;
		} else if (!transactionDate.equals(other.transactionDate))
			return false;
		return true;
	}

	/**
	 * @param close the close to set
	 */
	public void setClose(double close) {
		this.close = close;
	}

	/**
	 * @return the close
	 */
	public double getClose() {
		return close;
	}

	/**
	 * @param high the high to set
	 */
	public void setHigh(double high) {
		this.high = high;
	}

	/**
	 * @return the high
	 */
	public double getHigh() {
		return high;
	}

	/**
	 * @param low the low to set
	 */
	public void setLow(double low) {
		this.low = low;
	}

	/**
	 * @return the low
	 */
	public double getLow() {
		return low;
	}

	/**
	 * @param open the open to set
	 */
	public void setOpen(double open) {
		this.open = open;
	}

	/**
	 * @return the open
	 */
	public double getOpen() {
		return open;
	}

	/**
	 * @param transactionDate the transactionDate to set
	 */
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	/**
	 * @return the transactionDate
	 */
	public Date getTransactionDate() {
		return transactionDate;
	}
	
}
