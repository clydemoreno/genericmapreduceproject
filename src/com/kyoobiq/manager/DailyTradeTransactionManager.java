package com.kyoobiq.manager;


import com.kyoobiq.model.Account;
import com.kyoobiq.model.DailyTradeParameter;
import com.kyoobiq.model.MatrixModel;
import com.kyoobiq.utility.AppProperties;


public class DailyTradeTransactionManager {
	public static Account populateAccount(){
		AppProperties appProperties = AppProperties.getInstance();
        appProperties.load();
		
        float accountBalance = Float.parseFloat(appProperties.getProperty("accountBalance"));
        float accountAverageStopLoss = Float.parseFloat(appProperties.getProperty("accountAverageStopLoss"));
        float accountRiskPercentage = Float.parseFloat(appProperties.getProperty("accountRiskPercentage"));
		
		Account account = new Account();
		account.balance=accountBalance;
		account.averageStopLoss=accountAverageStopLoss;
		account.riskPercentage=accountRiskPercentage;
		return account;
	}
	public static void process(DailyTradeParameter param){
		float high=0,low=Float.MAX_VALUE,lots=0;
		MatrixModel model = new MatrixModel();

		model.setProfitTargetLotPercentage(0, 0.3f);
		model.setProfitTargetLotPercentage(1, 0.4f);
		model.setProfitTargetLotPercentage(2, 0.3f);
		model.setProfitTargetPercentage(0, 0.6f);
		model.setProfitTargetPercentage(1, 0.8f);
		model.setProfitTargetPercentage(2, 1.0f);
		
		Account account = param.account;

		for(int i = 0; i < 3; i++){
			account.setProfitTargetPercentage(i,model.getProfitTargetPercentage(i));
			account.setProfitTargetLotPercentage(i ,model.getProfitTargetLotPercentage(i));
		}
		AccountManager accountManager = AccountManager.getInstance();
		lots=accountManager.getLotSize(account);

//		AppProperties appProperties = AppProperties.getInstance();
//        appProperties.load();
        int asianStartHour = param.asianStartHour;
        int tradeStartHour = param.tradeStartHour;
		
		
	}
}
