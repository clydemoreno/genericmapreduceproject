package com.kyoobiq.manager;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.Iterator;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Queue;



import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.OutputCollector;
import com.kyoobiq.model.Account;
import com.kyoobiq.model.DailyTradeParameter;
import com.kyoobiq.model.DailyTradeTransaction;
import com.kyoobiq.model.MinMaxCountTuple;
import com.kyoobiq.model.Trade;
import com.kyoobiq.utility.Calculator;

public class MinMaxReduceManager {
	double lastClosePrice;

	@SuppressWarnings("deprecation")
	public void processReduce(Text key, Iterator<DailyTradeTransaction> values,
			OutputCollector<Text, DailyTradeTransaction> outputCollector, DailyTradeParameter param)
			throws IOException, ParseException {
		//initialize our result
		//MinMaxCountTuple result = new MinMaxCountTuple();
		DailyTradeTransaction result = new DailyTradeTransaction();
		result.setMaxPrice((float) 0.0);
		result.setMinPrice((float) 0.0);
		float sum = 0;
		float count = 0;
		
		List<DailyTradeTransaction> list = new ArrayList<DailyTradeTransaction>(); //(Queue<MinMaxCountTuple>) new ArrayList<MinMaxCountTuple>();
		//Iterate through all input values for this key
//		int j = 0;
//		int k = 0;
		while(values.hasNext()){
			DailyTradeTransaction val = (DailyTradeTransaction)values.next();
			//only get trades that starts with the trading hour all the way to the end of the day.  We will monitor if entry price has been hit, and also if profit targets and stop losses were hit
			//us daylight savings time
			if(val.getTransactionDate().getHours() >= param.getDaylightStartHour(val.getTransactionDate())){
				DailyTradeTransaction mm = new DailyTradeTransaction();
				
				copyObject(val, mm);
				
				list.add(mm);
//				System.out.println(val.getOpen());
//				System.out.println(list.get(j).getOpen());
//				j +=1;
				
			}
//			if(result.getMin() == null || val.getMin().compareTo(result.getMin()) < 0){
//				result.setMin(val.getMin());
//			}
//			if(result.getMax() == null || val.getMax().compareTo(result.getMax()) > 0){
//				result.setMax(val.getMax());
//			}
			//get high low
			getHighAndLow(result, val, param);
//			if(result.getMinPrice() == (float) 0.0 || val.getMinPrice() < (result.getMinPrice()) ){
//				result.setMinPrice(val.getMinPrice());
//			}
//			
//			if(result.getMaxPrice() == (float) 0.0 || val.getMaxPrice() > (result.getMaxPrice()) ){
//				result.setMaxPrice(val.getMaxPrice());
//			}
//			sum += (val.getCount() * val.getAverage());
//			count += val.getCount();

//			if(val.getTransactionDate().getHours() >= param.tradeStartHour){
//				System.out.println(val.getOpen());
//				System.out.println(list.get(k).getOpen());
//				k +=1;
//				
//			}
			
			
			
			
		}
		
		//for(MinMaxCountTuple m: list){
//			System.out.println(list.get(0).getOpen());
//			System.out.println(list.get(1).getOpen());
		//}
		
		 AccountManager accountManager = AccountManager.getInstance();
		 float lots=accountManager.getLotSize(param.account);

		for(int i=0;i<3;i++)
		{
			//create a trade entry only if lot size is not zero.
			
			populateDailyTradeTransactions(key,result.getMaxPrice(), result.getMinPrice(), lots, param.account, i);
			

		}
		
		//Clyde, look at this first before you debug the active = false.  maybe the reason a transaction became false was that it was set to false 
		//when a successful trade of either buy or sell.  Don't waste your time.
		monitorTrades(list, param, outputCollector);
		
		//set our final values
//		result.setCount(count);
//		result.setAverage(sum / count);
		
//		outputCollector.collect(key, result);
//		MinMaxCountTuple newResult = new MinMaxCountTuple();
//		newResult.setAverage(50);
//		newResult.setClose(3);
//		outputCollector.collect(key, newResult);
	}
	private void copyObject(DailyTradeTransaction val, DailyTradeTransaction mm) {
		mm.setTransactionDate(val.getTransactionDate());
		mm.setMaxPrice(val.getMaxPrice());
		mm.setMinPrice(val.getMinPrice());
		mm.setOpen(val.getOpen());
		mm.setHigh(val.getHigh());
		mm.setLow(val.getLow());
		mm.setClose(val.getClose());
	}
	@SuppressWarnings("deprecation")
	private void getHighAndLow(DailyTradeTransaction result, DailyTradeTransaction val, DailyTradeParameter param){
		//get the min from beginning of asian session to the end of asian session or the trade starting hour.
		//no need to filter the pre-asian session since the mapper already filters them out
		if(val.getTransactionDate().getHours() <= param.getDaylightStartHour(val.entryTime) 
				&& (result.getMinPrice() == (float) 0.0
				|| val.getMinPrice() < (result.getMinPrice())) ){
			result.setMinPrice(val.getMinPrice());
		}
		if(val.getTransactionDate().getHours() <= param.getDaylightStartHour(val.getTransactionDate()) && (result.getMaxPrice() == (float) 0.0 || val.getMaxPrice() > (result.getMaxPrice()) )){
			result.setMaxPrice(val.getMaxPrice());
		}
		
	}
	private void populateDailyTradeTransactions(Text key, double d, double e,
			float lots, Account account, int i) {
		DailyTradeTransaction buyEntry = new DailyTradeTransaction();
		buyEntry.tradeNumber = i + 1;
		buyEntry.id = key.toString();
		DailyTradeTransaction sellEntry = new DailyTradeTransaction();
		sellEntry.tradeNumber = i + 1;
		sellEntry.id = key.toString();
		
		buyEntry.profitTartgetPercentage = account.getProfitTargetPercentage(i);
		buyEntry.entry=d;
		buyEntry.type=1;
		
		buyEntry.lotSize = (lots * account.getProfitTargetLotPercentage(i));
		buyEntry.lotSizePercentage = account.getProfitTargetPercentage(i);
		buyEntry.profitTarget=Calculator.getProfitTarget(buyEntry.type, d, e, buyEntry.profitTartgetPercentage);
		buyEntry.stopLoss = Calculator.getStopLoss(d, e);
		buyEntry.isEntryPriceGotHit=false;
		buyEntry.active=true;
		buyEntry.isProfitTargetHit=false;
		buyEntry.isStopLossGotHit=false;
		
		sellEntry.profitTarget=Calculator.getProfitTarget(buyEntry.type, d, e, buyEntry.profitTartgetPercentage);
		sellEntry.profitTartgetPercentage = account.getProfitTargetPercentage(i);
		sellEntry.entry=e;
		sellEntry.type=2;
		sellEntry.lotSize = (lots * account.getProfitTargetLotPercentage(i));
		sellEntry.lotSizePercentage=account.getProfitTargetLotPercentage(i);
		sellEntry.profitTarget= Calculator.getProfitTarget(sellEntry.type, d, e, buyEntry.profitTartgetPercentage);
		sellEntry.stopLoss= Calculator.getStopLoss(d, e);
		sellEntry.isEntryPriceGotHit=false;
		sellEntry.active=true;
		sellEntry.isProfitTargetHit=false;
		sellEntry.isStopLossGotHit=false;

		if(account.getProfitTargetLotPercentage(i) > 0.0f){
			account.buyList.add(buyEntry);
			account.sellList.add(sellEntry);		
		}
	}
//	monitorTrades(param.account,t, year, month, day, tradeStartHour, tradeEndHour,model);

	private void monitorTrades(List<DailyTradeTransaction> list, DailyTradeParameter param, OutputCollector<Text, DailyTradeTransaction> outputCollector) throws IOException, ParseException{
		//access via Iterator
//		Iterator iterator = list.iterator();
//		while(iterator.hasNext()){
//			MinMaxCountTuple element = (MinMaxCountTuple) iterator.next();
//			System.out.println( element.getOpen());
//		}
		SimpleDateFormat frmt =  MinMaxCountTuple.frmt;

		for(DailyTradeTransaction val : list){
			Trade trade = new Trade();
			
			trade.id = frmt.format(val.getTransactionDate());
			trade.close = val.getClose();
			trade.high = val.getHigh();
			trade.open = val.getOpen();
			trade.low = val.getLow();
			
			checkTrades(trade,param,outputCollector);
		}
		
		handleEndOfTradingSession(param.account, lastClosePrice,outputCollector);

	}
	private void handleEndOfTradingSession(Account account, double lastClosePrice, OutputCollector<Text, DailyTradeTransaction> outputCollector) throws IOException {
		for(DailyTradeTransaction buyDailyTradeTransaction:account.buyList)
		{
//			System.out.println(String.format("Buy Entry Time: %s Exit Time: %s Entry Price:%s StopLoss:%s ProfitTarget:%s ProfitPercentage:%s Lots:%s LotsPercentage:%s Profit:%s",buyDailyTradeTransaction.entryTime,buyDailyTradeTransaction.exitTime, buyDailyTradeTransaction.entry,buyDailyTradeTransaction.stopLoss,buyDailyTradeTransaction.profitTarget,buyDailyTradeTransaction.profitTartgetPercentage, buyDailyTradeTransaction.lotSize, buyDailyTradeTransaction.lotSizePercentage,buyDailyTradeTransaction.profit));
			if(buyDailyTradeTransaction.active && buyDailyTradeTransaction.isStopLossGotHit==false && buyDailyTradeTransaction.isProfitTargetHit==false && buyDailyTradeTransaction.isEntryPriceGotHit)
			{
				buyDailyTradeTransaction.profit=lastClosePrice-buyDailyTradeTransaction.entry;
				buyDailyTradeTransaction.active=false;
				buyDailyTradeTransaction.type = 1;
				
				outputCollector.collect(new Text(buyDailyTradeTransaction.id),buyDailyTradeTransaction);
			}
			if(buyDailyTradeTransaction.active==false && buyDailyTradeTransaction.isEntryPriceGotHit==false)
			{
				buyDailyTradeTransaction.active=false;
			}
		}
	
		for(DailyTradeTransaction sellDailyTradeTransaction:account.sellList)
		{
			
//			System.out.println(String.format("Sell Entry Time: %s Exit Time: %s Entry Price:%s StopLoss:%s ProfitTarget:%s ProfitPercentage:%s Lots:%s LotsPercentage:%s Profit:%s",sellDailyTradeTransaction.entryTime, sellDailyTradeTransaction.exitTime, sellDailyTradeTransaction.entry,sellDailyTradeTransaction.stopLoss,sellDailyTradeTransaction.profitTarget,sellDailyTradeTransaction.profitTartgetPercentage, sellDailyTradeTransaction.lotSize, sellDailyTradeTransaction.lotSizePercentage,sellDailyTradeTransaction.profit));
			if(sellDailyTradeTransaction.active && sellDailyTradeTransaction.isStopLossGotHit==false && sellDailyTradeTransaction.isProfitTargetHit==false && sellDailyTradeTransaction.isEntryPriceGotHit)
			{
				sellDailyTradeTransaction.profit=sellDailyTradeTransaction.entry-lastClosePrice;
				sellDailyTradeTransaction.active=false;
//				MinMaxCountTuple newResult = new MinMaxCountTuple();
//				newResult.setAverage(buyDailyTradeTransaction.profit);
				sellDailyTradeTransaction.type = 2;
				
				outputCollector.collect(new Text(sellDailyTradeTransaction.id),sellDailyTradeTransaction);

			}
			if(sellDailyTradeTransaction.active==false && sellDailyTradeTransaction.isEntryPriceGotHit==false)
			{
				sellDailyTradeTransaction.active=false;
			}
		}
	}
	

	
	public void disableActiveTradesWhenOtherTradeIsSuccessful(List<DailyTradeTransaction> tradeList)
	{
		for(DailyTradeTransaction tradeTransaction:tradeList)
		{
			tradeTransaction.active=false;
		}
	}
	private void checkTrades(Trade trade, DailyTradeParameter param, OutputCollector<Text, DailyTradeTransaction> outputCollector) throws IOException, ParseException{
		
		if(trade!=null){
			SimpleDateFormat frmt =  MinMaxCountTuple.frmt;
			lastClosePrice=trade.close;
			for(DailyTradeTransaction buyDailyTradeTransaction:param.account.buyList)
			{
				if(buyDailyTradeTransaction.isEntryPriceGotHit==false && buyDailyTradeTransaction.active==true)
				{
					if(trade.open>=buyDailyTradeTransaction.entry || trade.high>=buyDailyTradeTransaction.entry)
					{
						buyDailyTradeTransaction.isEntryPriceGotHit=true;
						buyDailyTradeTransaction.entryTime  = frmt.parse(trade.id);
					}
				}
				if(buyDailyTradeTransaction.isEntryPriceGotHit==true && buyDailyTradeTransaction.active==true)
				{
					if(trade.open<=buyDailyTradeTransaction.stopLoss || trade.low<=buyDailyTradeTransaction.stopLoss)
					{
						buyDailyTradeTransaction.isStopLossGotHit=true;
						buyDailyTradeTransaction.exitTime  = frmt.parse(trade.id);
						buyDailyTradeTransaction.active=false;
						buyDailyTradeTransaction.profit = Calculator.calculateProfit(buyDailyTradeTransaction);
					
//						MinMaxCountTuple newResult = new MinMaxCountTuple();
//						newResult.setAverage(buyDailyTradeTransaction.profit);
						buyDailyTradeTransaction.type = 1;
						outputCollector.collect(new Text( buyDailyTradeTransaction.id),buyDailyTradeTransaction);
						//profitarget-entry=3.5*10=35 35*11.99
					}
					if(buyDailyTradeTransaction.active && (trade.open>=buyDailyTradeTransaction.profitTarget || trade.high>=buyDailyTradeTransaction.profitTarget))
					{
						buyDailyTradeTransaction.isProfitTargetHit=true;
						buyDailyTradeTransaction.exitTime  = frmt.parse(trade.id);
						buyDailyTradeTransaction.active=false;
						buyDailyTradeTransaction.profit=((buyDailyTradeTransaction.profitTarget-buyDailyTradeTransaction.entry))*buyDailyTradeTransaction.lotSize;
//						MinMaxCountTuple newResult = new MinMaxCountTuple();
//						newResult.setAverage(buyDailyTradeTransaction.profit);
//						DailyTradeTransaction newResult = new DailyTradeTransaction();
						buyDailyTradeTransaction.type = 1;
						outputCollector.collect(new Text( buyDailyTradeTransaction.id),buyDailyTradeTransaction);
	
						disableActiveTradesWhenOtherTradeIsSuccessful(param.account.sellList);
					}
				}
				//check stoploss is hit
			}
			
			for(DailyTradeTransaction sellDailyTradeTransaction:param.account.sellList)
			{
				if(sellDailyTradeTransaction.isEntryPriceGotHit==false && sellDailyTradeTransaction.active==true)
				{
					if(trade.open<=sellDailyTradeTransaction.entry || trade.low<=sellDailyTradeTransaction.entry)
					{
						sellDailyTradeTransaction.isEntryPriceGotHit=true;
					
						sellDailyTradeTransaction.entryTime = frmt.parse(trade.id);
					}
				}
				if(sellDailyTradeTransaction.isEntryPriceGotHit==true && sellDailyTradeTransaction.active==true)
				{
					if(trade.open>=sellDailyTradeTransaction.stopLoss || trade.high>=sellDailyTradeTransaction.stopLoss)
					{
						sellDailyTradeTransaction.isStopLossGotHit=true;
						sellDailyTradeTransaction.active=false;
						sellDailyTradeTransaction.exitTime = frmt.parse(trade.id);
						sellDailyTradeTransaction.profit=((sellDailyTradeTransaction.entry - sellDailyTradeTransaction.stopLoss)) * sellDailyTradeTransaction.lotSize;

//						MinMaxCountTuple newResult = new MinMaxCountTuple();
//						newResult.setAverage(buyDailyTradeTransaction.profit);
						sellDailyTradeTransaction.type = 2;
						outputCollector.collect(new Text( sellDailyTradeTransaction.id),sellDailyTradeTransaction);

						//profitarget-entry=3.5*10=35 35*11.99
					}
					if(sellDailyTradeTransaction.active && (trade.open<=sellDailyTradeTransaction.profitTarget || trade.low<=sellDailyTradeTransaction.profitTarget))
					{
						sellDailyTradeTransaction.isProfitTargetHit=true;
						sellDailyTradeTransaction.exitTime = frmt.parse(trade.id);
						sellDailyTradeTransaction.active=false;
						sellDailyTradeTransaction.profit=((sellDailyTradeTransaction.entry-sellDailyTradeTransaction.profitTarget))*sellDailyTradeTransaction.lotSize;

//						MinMaxCountTuple newResult = new MinMaxCountTuple();
//						newResult.setAverage(buyDailyTradeTransaction.profit);
						sellDailyTradeTransaction.type = 2;
						outputCollector.collect(new Text( sellDailyTradeTransaction.id),sellDailyTradeTransaction);
						
						disableActiveTradesWhenOtherTradeIsSuccessful(param.account.buyList);
					}
				}
				//check stoploss is hit
			}
		}
		
	}
}
