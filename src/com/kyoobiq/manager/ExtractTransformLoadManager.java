package com.kyoobiq.manager;

import java.util.HashMap;
import java.util.Hashtable;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.MapReduceDriver;

import com.kyoobiq.model.DailyTradeTransaction;
import com.kyoobiq.model.MinMaxCountTuple;
import com.kyoobiq.model.Trade;
import com.kyoobiq.utility.AppProperties;


public class ExtractTransformLoadManager {
	   public Hashtable<String, Trade> transform(){
			AppProperties appProperties = AppProperties.getInstance();
	        appProperties.load();
	        String csvFile = appProperties.getProperty("filePath");
	    	//String csvFile = "/Users/clydemoreno/Downloads/nadex/gold2007-1-18.csv";
	    	BufferedReader br = null;
	    	String line = "";
	    	String cvsSplitBy = ",";
	    	Hashtable<String, Trade> table = null;
	    	try {
	    		table = new Hashtable<String, Trade>();
	    		//date, time, open, high, low, close, volume
	    		//2012.01.06,22:00,1617.02000,1617.02000,1615.50000,1616.27000,676
	    		br = new BufferedReader(new FileReader(csvFile));
	    		Trade t = null;
	    		while ((line = br.readLine()) != null) {
	     
	    		        // use comma as separator
	    			String[] trades = line.split(cvsSplitBy);
	    			System.out.println(String.format("Date: %s Time: %s ",trades[0], trades[1]));
	     
	    			
	    			if(trades.length == 7){
	    				String endProcessingDate = appProperties.getProperty("endProcessingDate");
	    				String id = String.format("%s,%s",trades[0], trades[1]);	
	    				//THIS WILL CODE WILL STOP READING THE FILE WHEN IT FINDS THE END OF PROCESSING DATE
	    				if(id.substring(0,7).equals(endProcessingDate)){
//	       					System.out.print("this should  show up once\n");
	    					break;
	    				}
	    				t = new Trade();
	        			System.out.println(String.format("Trade was instantiated: Date: %s Time: %s ",trades[0], trades[1]));
	    				t.id = id;
	    				t.open = Float.parseFloat(trades[2]);
	    				t.high = Float.parseFloat(trades[3]);
	    				t.low = Float.parseFloat(trades[4]);
	    				t.close = Float.parseFloat(trades[5]);
	    				
	    				//possibly check if within the months stated
	    				//System.out.print(t.id.substring(0,7).equals("2007.05"));
	    				
	   				    table.put(t.id,t );
	    				
	    			}
	    			
	    		}
	     
	    	} catch (FileNotFoundException e) {
	    		e.printStackTrace();
	    	} catch (IOException e) {
	    		e.printStackTrace();
	    	} finally {
	    		if (br != null) {
	    			try {
	    				br.close();
	    			} catch (IOException e) {
	    				e.printStackTrace();
	    			}
	    		}
	    	}
	    	return table;
	    }
	
    public Hashtable<String, Trade> extract(MapReduceDriver<Object, Text, Text, DailyTradeTransaction, Text, DailyTradeTransaction> dm){
		AppProperties appProperties = AppProperties.getInstance();
        appProperties.load();
        String csvFile = appProperties.getProperty("filePath");
    	//String csvFile = "/Users/clydemoreno/Downloads/nadex/gold2007-1-18.csv";
    	BufferedReader br = null;
    	String line = "";
    	String cvsSplitBy = ",";
    	Hashtable<String, Trade> table = null;
    	try {
    		table = new Hashtable<String, Trade>();
    		//date, time, open, high, low, close, volume
    		//2012.01.06,22:00,1617.02000,1617.02000,1615.50000,1616.27000,676
    		br = new BufferedReader(new FileReader(csvFile));
    		Trade t = null;
    		int i = 0;
    		while ((line = br.readLine()) != null) {
 	  			dm.withInput(new LongWritable(i + 1),new Text(line));
 	  		    			String[] trades = line.split(cvsSplitBy);
     
    			
   				    
   				    
    				
    			
    			
    		}
     
    	} catch (FileNotFoundException e) {
    		e.printStackTrace();
    	} catch (IOException e) {
    		e.printStackTrace();
    	} finally {
    		if (br != null) {
    			try {
    				br.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    	}
    	return table;
    }

    public HashMap<String, String> transformTest(){
		AppProperties appProperties = AppProperties.getInstance();
        appProperties.load();
        String csvFile = appProperties.getProperty("filePath");
    	//String csvFile = "/Users/clydemoreno/Downloads/nadex/gold2007-1-18.csv";
    	BufferedReader br = null;
    	String line = "";
    	String cvsSplitBy = ",";
//    	Hashtable<String, Trade> table = null;
    	HashMap<String, String> tab = new HashMap<String, String>();
    	try {
//    		table = new Hashtable<String, Trade>();
    		//date, time, open, high, low, close, volume
    		//2012.01.06,22:00,1617.02000,1617.02000,1615.50000,1616.27000,676
    		br = new BufferedReader(new FileReader(csvFile));
    		Trade t = null;
    		int i = 0;
    		String fileName = "";
    		String temp = "";
    		
    		while ((line = br.readLine()) != null) {
//    			System.out.println(String.format("Line: %s ",line));
     
    		        // use comma as separator
    			String[] trades = line.split(cvsSplitBy);
    			
    			if(trades.length == 7){
        			System.out.println(String.format("Date: %s Time: %s  count:%d",trades[0], trades[1], i++));
    				String id = String.format("%s,%s",trades[0], trades[1]);	
        			fileName = String.format("%sTrades.csv", trades[0].substring(0, 4));
        			if(!tab.containsKey(fileName)){
        				tab.put(fileName, fileName);
        			}
        			
//    				if(!temp.equals("") || !temp.equals(fileName)){
//    					temp = fileName;
//    				}
        			//add another way to write by passing actual filename
        			String values = String.format("%s,%s,%s,%s\n",trades[2], trades[3], trades[4], trades[5]);
//    				LogManager.write("yearlyTradeFilePath", values,fileName);
//        			t = new Trade();
        			
    			}    			
//    			if(trades.length == 7){
//    				String endProcessingDate = appProperties.getProperty("endProcessingDate");
//    				String id = String.format("%s,%s",trades[0], trades[1]);	
//    				//THIS WILL CODE WILL STOP READING THE FILE WHEN IT FINDS THE END OF PROCESSING DATE
//    				if(id.substring(0,7).equals(endProcessingDate)){
////       					System.out.print("this should  show up once\n");
//    					break;
//    				}
//    				t = new Trade();
//        			System.out.println(String.format("Trade was instantiated: Date: %s Time: %s ",trades[0], trades[1]));
//    				t.id = id;
//    				t.open = Float.parseFloat(trades[2]);
//    				t.high = Float.parseFloat(trades[3]);
//    				t.low = Float.parseFloat(trades[4]);
//    				t.close = Float.parseFloat(trades[5]);
//    				
//    				//possibly check if within the months stated
//    				//System.out.print(t.id.substring(0,7).equals("2007.05"));
//    				
//   				    table.put(t.id,t );
//    				
//    			}
    			
    		}
     
    	} catch (FileNotFoundException e) {
    		e.printStackTrace();
    	} catch (IOException e) {
    		e.printStackTrace();
    	} finally {
    		if (br != null) {
    			try {
    				br.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    	}
    	return tab;
    }
}
